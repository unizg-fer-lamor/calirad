# calirad
-----

This ROS toolbox is an implementation of trackig-based calibration  accompanying the [Spatiotemporal Multisensor Calibration via Gaussian Processes Moving Target Tracking](https://arxiv.org/abs/1904.04187) paper. 

The toolbox can be used with various sensor combinations, as long as both sensors can provide a 3D position of a tracked object.
The output of the toolbox is:

 - an extrinsic calibration given by TF2 frames and through ROS parameter server,
 - time delay between the sensors (based on timestamps) given through ROS parameter server. 

 The toolbox is based on Exactly Sparse Gaussian Process Regression implemented by [ESGPR](http://www.bitbucket.org/unizg-fer-lamor/esgpr).

-----

### PREREQUISITES

##### [ESGPR](http://www.bitbucket.org/unizg-fer-lamor/esgpr)

##### [Ceres](http://ceres-solver.org/)
[Installation instructions](http://ceres-solver.org/installation.html)
##### [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page)
Install with:
```
$sudo apt install libeigen3-dev
```
##### [ROS](http://www.ros.org/)

### USAGE

The toolbox is organized as a metapackage, while the calibration is done within the *track_calibration_node* node in the *calirad_core* package.
The node subscribes to two topics, *track3_array_sensor1* and *track3_array_sensor2*, of the type track_msgs/Track3Array. 
These messages carry information about the tracked objects. 
In the current implementation ID-s given to the tracked object should be matched between the sensors before the calibration node. 
Based on the position information and matched ID-s given in the messages, the calibration logs the data and manages the segments to perform Gaussian Process Regression.
When new matches between the segments are found, the calibration is run.

The calibration can be controlled with several parameters (see calirad_core/config/calibration_parameters.yaml). 
The initial time delay can be set thorugh parameter. However, if the option is disabled, the initial time delay will be estimated based on a comparison of the message arrival times and the timestamps, which is usually sufficent. 

### EXAMPLE 

An example of the calibration can be run with:

```
$roslaunch calirad_core track_calibration.launch
```

The launch file starts the bag file obtained by detecting an [AprilTag](https://april.eecs.umich.edu/software/apriltag) with two cameras. 
The detections are converted to Track3Array messages and fed to the *track_calibration_node*.
Markers are visualized in the Rviz, while the calibration modifes TF frames after 10s (segment length set to 10s). 
The estimated extrinsic and temporal parameters are also avaiable through the ROS parameter server. 


### CITING

If you use calirad in an academic context, please cite the following publication:

```
@article{Persic21preprint,
  author    = {Juraj Persic and Luka Petrovic and Ivan Markovic and Ivan Petrovic},
  title     = {Spatiotemporal Multisensor Calibration via Gaussian Processes Moving Target Tracking},
  year      = {2021},
  url       = {https://lamor.fer.hr/images/50020776/Persic2019.pdf}
}
```
-----

### CONTRIBUTORS
Juraj Peršić and Luka Petrović who are with the [Laboratory for Autonomous Systems and Mobile Robotics (LAMOR)](https://lamor.fer.hr).

This work has been supported by the European Regional Development Fund under the project System for increased driving safety in public urban rail traffic (SafeTRAM). This research has also been carried out within the activities of the Centre of Research Excellence for Data Science and Cooperative Systems supported by the Ministry of Science and Education of the Republic of Croatia.

-----
### LICENSE

calirad is released under the GPLv3 license, reproduced in the file [LICENSE.txt](https://bitbucket.org/unizg-fer-lamor/calirad/src/master/LICENSE.txt) in the directory.

### COMPATIBILITY

The toolbox is developed and tested under Ubuntu 18.04 and ROS Melodic.
