/**
 *  This file is a part of calirad.
 *
 *  Copyright (C) 2018 Juraj Persic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  calirad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CALRAD_MATCHING_H
#define CALRAD_MATCHING_H

#include <Eigen/Dense>
#include <calirad_core/track_log.h>
#include <fstream>
#include <unordered_map>
#include <utility>
#include <vector>

namespace calirad {
#define CALIRAD_MATCH_TIME_DIFF_THRESHOLD 0.05

/** \brief Class that holds information on matched correspondences between
sensors.
*
MatchElement class contains indices to matched trackData between sensor
TrackLogs.
*/
class MatchElement {
public:
  MatchElement(std::pair<int, int> index_sensor1,
               std::pair<int, int> index_sensor2, std::pair<int, int> segments);
  std::pair<int, int> getIndexSensor1();
  std::pair<int, int> getIndexSensor2();
  std::pair<int, int> getIndexSegments();

private:
  std::pair<int, int> index_sensor1_;
  std::pair<int, int> index_sensor2_;
  std::pair<int, int> segments_;
};

/** \brief Class that holds information on interpolated correspondences between
sensors.
*
MatchElement class contains indices to matched trackData between sensor
TrackLogs.
*/
class InterpolatedElement {
public:
  InterpolatedElement(const Eigen::MatrixXd &state_sensor1,
                      const Eigen::MatrixXd &state_sensor2);
  void getStates(Eigen::MatrixXd *state_sensor1,
                 Eigen::MatrixXd *state_sensor2);
  void getPositions(Eigen::Vector3d *position_sensor1,
                    Eigen::Vector3d *position_sensor2);
  double getVelocityNormResidual();
  double getVelocityNormJacobian();
  // Eigen::MatrixXd getVelocityResidual();

private:
  Eigen::MatrixXd state_sensor1_;
  Eigen::MatrixXd state_sensor2_;
};

class PairedSegments {
public:
  PairedSegments();
  PairedSegments(const int segment_index_sensor1,
                 const int segment_index_sensor2);
  int segment_index_sensor1_;
  int segment_index_sensor2_;

  int start_index_sensor1_;
  int end_index_sensor1_;
  double start_time_sensor1_;
  double end_time_sensor1_;
  int start_index_sensor2_;
  double start_time_sensor2_;
  double common_start_time_;
  double common_end_time_;

private:
};

/** \brief Class that matches Segments.
*
MatchedSegments class creates matches between segments through a number of
possible algorithms: simpleMatching(), simpleGPMatching(),...
*/
class MatchedSegments {
public:
  MatchedSegments();
  MatchedSegments(std::shared_ptr<TrackLog> log_sensor1,
                  std::shared_ptr<TrackLog> log_sensor2);
  void clearMatches();
  /**
  Method simpleMatching() creates matching by assuming zero time-delay and
  connects sensor reading to first closest correspondence. Consistent track id-s
  are assumed to be already matched.
  */
  void simpleMatching(TrackLog &log_sensor1, TrackLog &log_sensor2);
  void simpleGPMatching();
  void interpolateMatchesGP();
  double getTimeDelay();
  double getInitialTimeDelay();
  // double getTotalTimeDelay();
  void setTimeDelay(const double &time_delay);
  void setTimeDelayBounds(const double &lower, const double &upper);
  void setInitialTimeDelay(const double &initial_time_delay);
  void setExcludeOutliers(bool exclude_outliers);
  bool getExcludeOutliers();
  std::vector<MatchElement> matches_;
  std::vector<InterpolatedElement> interpolated_matches_;
  std::vector<PairedSegments> segment_pair_list_;

private:
  /**
  time_delay_ is estimated, while the initial_time_delay is kept constant.
  Even though they are both added to sensor2 timestamps, they are separated
  for conveninece.
  Time delay initialization can be done roughly by comparing timestamps of
  messages with close arrive time.
  Afterwards, lower and upper bounds limit the interval of time_delay_
  around zero.
  */
  double time_delay_;
  double initial_time_delay_;
  double time_delay_lower_bound_;
  double time_delay_upper_bound_;
  std::shared_ptr<TrackLog> log_sensor1_;
  std::shared_ptr<TrackLog> log_sensor2_;
  bool exclude_outliers_;
};

} // namespace calirad

#endif