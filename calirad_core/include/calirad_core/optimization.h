/**
 *  This file is a part of calirad.
 *
 *  Copyright (C) 2018 Juraj Persic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  calirad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CALRAD_OPTIMIZATION_H
#define CALRAD_OPTIMIZATION_H

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <calirad_core/matching.h>
#include <ceres/ceres.h>
#include <ceres/rotation.h>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>

namespace calirad {

/** \brief Class that optimizes transform based on velocities and position.
*
TrackData contains basic information about a unique track determined by an ID
and it is part of a higher class TrackFrame.
*/

/////// Find rotation and translation by minimizing position reprojection error
/// an using SVD-based algorithm
class ExtrinsicCalibration {
public:
  ExtrinsicCalibration(std::shared_ptr<MatchedSegments> matched_segments);
  void getTranslation(std::vector<double> *translation);
  void getAngleAxis(std::vector<double> *angle_axis);
  void getAngleAxisEigen(Eigen::AngleAxis<double> *angle_axis_eigen);
  void solveSVD();
  // Eigen::Vector3d getEulerAngles();
  // Eigen::Vector3d getAngleAxis();
  // void getAngleAxis(Eigen::Vector3d *angle_axis);
  // void getTranslation(Eigen::Vector3d *translation);
  // void getQuaternion(double *q);

private:
  std::shared_ptr<MatchedSegments> matched_segments_;
  std::vector<double> angle_axis_;
  Eigen::AngleAxis<double> angle_axis_eigen_;
  std::vector<double> translation_;
};

/////// Find time delay by minimizing velocity norm error
class TimeDelayOpt {
public:
  TimeDelayOpt(std::shared_ptr<MatchedSegments> matched_segments);
  void setInitialTimeDelay(const double &initial_time_delay);
  void setBounds(double &lower_bound, double &upper_bound);
  void setOptimizationParameters(double function_tolerance,
                                 double gradient_tolerance,
                                 double parameter_tolerance);
  void addCorrespondence();
  void solve();
  double getTimeDelay();
  double getFinalCost();

private:
  ceres::Problem problem_;
  ceres::Solver::Options options_;
  ceres::Solver::Summary summary_;
  ceres::Problem problem_analytical_;
  ceres::Solver::Options options_analytical_;
  ceres::Solver::Summary summary_analytical_;
  double time_delay_;
  double time_delay_analytical_;
  double lower_bound_;
  double upper_bound_;
  std::shared_ptr<MatchedSegments> matched_segments_;

  struct VelocityDelayResidual {
    VelocityDelayResidual(std::shared_ptr<MatchedSegments> matched_segments)
        : matched_segments_(matched_segments) {}

    template <typename T>
    bool operator()(const T *const time_delay, T *residual) const {

      if (*time_delay != matched_segments_->getTimeDelay()) {
        matched_segments_->setTimeDelay(*time_delay);
        matched_segments_->interpolateMatchesGP();
      }

      for (size_t i = 0; i < matched_segments_->interpolated_matches_.size();
           i++) {

        residual[i] = matched_segments_->interpolated_matches_[i]
                          .getVelocityNormResidual();
      }
      return true;
    }

  private:
    // Observations for a sample.
    const Eigen::Vector3d point_sensor1_;
    const Eigen::Vector3d point_sensor2_;
    std::shared_ptr<MatchedSegments> matched_segments_;
  };
};
class AnalyticalTimeDelayCostFunction : public ceres::CostFunction {
public:
  AnalyticalTimeDelayCostFunction(
      std::shared_ptr<MatchedSegments> matched_segments);
  virtual bool Evaluate(const double *const *time_delay, double *residuals,
                        double **jacobians) const;

private:
  std::shared_ptr<MatchedSegments> matched_segments_;
};
} // namespace calirad

#endif