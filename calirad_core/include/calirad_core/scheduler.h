/**
 *  This file is a part of calirad.
 *
 *  Copyright (C) 2018 Juraj Persic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  calirad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CALRAD_SCHEDULER_H
#define CALRAD_SCHEDULER_H

#include "calirad_core/matching.h"
#include "calirad_core/optimization.h"
#include "calirad_core/track_log.h"
#include "ros/ros.h"
#include <boost/filesystem.hpp>
#include <chrono>
#include <ctime>
namespace fs = boost::filesystem;
#include <fstream>
#include <geometry_msgs/TransformStamped.h>
#include <iomanip>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <tf2/LinearMath/Transform.h>

#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/transform_listener.h>

namespace calirad {

/** \brief Class that handles tasks: data collection, calibration.
*
Scheduler is a class which implements subscribers to sensors that log the data
using the TrackLog class.
Additionally, it uses ros::Timer to periodically perform calibration on
available data.
Finally, it exports the resut of calibration through tf2::Transform.
*/
class Scheduler {
public:
  Scheduler(ros::NodeHandle nh, std::string subscribed_topic_sensor1,
            std::string subscribed_topic_sensor2);
  void setTimerDuration(ros::Duration duration);
  void broadcastEstimatedTransform();

private:
  void timerCallback(const ros::TimerEvent &event);
  void setTF2Transform();
  double time_delay_;
  double initial_time_delay_;
  double total_time_delay_;
  double time_delay_lower_bound_;
  double time_delay_upper_bound_;
  bool initial_time_delay_set_;
  bool publish_tf_;
  double calibration_update_time_;
  ros::Timer timer_;
  ros::NodeHandle nh_;
  std::shared_ptr<TrackLog> log_sensor1_;
  std::shared_ptr<TrackLog> log_sensor2_;
  std::shared_ptr<MatchedSegments> matched_segments_;
  tf2::Transform s1_to_s2_transform_;
  tf2_ros::TransformBroadcaster br_;
  tf2_ros::Buffer tf_buffer_;
  std::vector<double> translation_;
  std::vector<double> angle_axis_;
  Eigen::AngleAxis<double> angle_axis_eigen_;
  std::ofstream output_file_;
};
} // namespace calirad

#endif // CALRAD_SCHEDULER_H