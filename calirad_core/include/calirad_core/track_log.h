/**
 *  This file is a part of calirad.
 *
 *  Copyright (C) 2018 Juraj Persic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  calirad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CALRAD_TRACK_LOH_H
#define CALRAD_TRACK_LOH_H

#include "track_msgs/Track3Array.h"
#include <Eigen/Dense>
#include <chrono>
#include <esgpr/core/gp_regression.h>
#include <esgpr/model/constant_acceleration3.h>
#include <fstream>
#include <memory>
#include <ros/ros.h>
#include <unordered_map>
#include <vector>

namespace calirad {

/** \brief Class that logs data about a single track in a signle time frame.
*
TrackData contains basic information about a unique track determined by an ID
and it is part of a higher class TrackFrame.
*/
class TrackData {
public:
  TrackData();
  TrackData(const track_msgs::Track3 &track);
  TrackData(const track_msgs::Track3 &track, const double time);
  Eigen::Vector3d getPos() const;
  Eigen::Vector3d getVel() const;
  Eigen::Vector3d getAcc() const;
  double getTime() const;
  int getStatus() const;
  int getId() const;
  void setPos(const double x, const double y, const double z);
  void setVel(const double x, const double y, const double z);
  void setAcc(const double x, const double y, const double z);
  void setTime(const double time);
  void setStatus(const int status);
  void setId(const int id);

private:
  Eigen::Vector3d pos_;
  Eigen::Vector3d vel_;
  Eigen::Vector3d acc_;
  double time_;
  int status_;
  int id_;
};

/** \brief Class that logs data about all tracks in a signle time frame.
*
TrackFrame contains a vector of TrackData elements which are found wihtin the
time frame available by getTime() method.
*/
class TrackFrame {
public:
  TrackFrame();
  TrackFrame(const track_msgs::Track3Array::ConstPtr &track_array);
  std::shared_ptr<TrackData> getTrackPtr(int track_index);
  double getTime();
  void setTime(double time);
  int getTrackNumber();

private:
  std::vector<TrackData> track_;
  int track_number_;
  double time_;
};

/** \brief Class that connects mulitple TrackData through time.
*
Segment class provides a timeseries that provides links to TrackData about a
unique track. Method linkTrackData() attaches a new TrackData defined by frame
index and track index. Method getFrameTrackIndex() returns the same index pair
(frame + track) for the n-th element of the Segment.
*/
class Segment {
public:
  Segment(const std::shared_ptr<TrackData> track_data_ptr);
  void linkTrackData(const std::shared_ptr<TrackData> track_data_ptr);
  double getStartTime() const;
  double getEndTime() const;
  double getMeasurementTime(const int index);
  std::pair<int, int> getFrameTrackIndex(int frame_track_index) const;
  int getSegmentLength() const;
  int getId() const;
  void GPRegression();
  esgpr::GPReg<esgpr::ConstAcc3> gp_regression_;

private:
  double start_time_;
  double end_time_;
  std::vector<std::pair<int, int>> frame_track_index_;
  std::vector<std::shared_ptr<TrackData>> track_data_ptr_;
  int length_;
  int id_;
  bool is_active_;
};

/** \brief Class that logs track data and handles it through Segments.
*
TrackLog class subscribes to topic <subscribed_topic> of type
track_msgs::Track3Array and logs each frame wihtin the trackCallback method.
Within the callback, ROS message is copied to TrackFrame object and appended to
the end of the vector frame_ which logs all the track frames.
Afterwards, updateSegment() method tries to append track data of individual
track
of corresponding active_segments_; if not possible, it initiates new segments.
Lastly, manageSegments() moves active segments to finished if they are too long
or timeout occured. After a segment is declared finished, Gaussian Process (GP)
regresion if estimated for it.
*/
class TrackLog {
public:
  TrackLog(std::string subscribed_topic, double timeout, double max_duration);
  std::shared_ptr<TrackFrame> getFramePtr(int frame_index);
  int getFrameNumber();
  std::shared_ptr<Segment> getSegmentPtr(int segment_index);
  int getSegmentNumber();
  void setTimeout(const double &timeout);
  void setMaxDuration(const double &max_duration);
  double getLastFrameTimestamp();
  double getLastFrameArrivalTime();
  void setQc(std::vector<double> &q_c);
  void setRk(std::vector<double> &r_k);
  void setP0(std::vector<double> &p_0);
  void setSensorName(std::string &sensor_name);
  void getSensorName(std::string *sensor_name);
  std::shared_ptr<TrackData> getTrackPtr(std::pair<int, int> frame_track_index);
  std::string getFrameId();

private:
  void trackCallback(const track_msgs::Track3Array::ConstPtr &tracks_msg);
  void updateSegments(TrackFrame &track_frame, const int frame_index);
  void manageSegments();
  void timerCallback(const ros::TimerEvent &event);
  ros::Timer timer_;
  std::vector<TrackFrame> frame_;
  ros::Subscriber sub_;
  ros::NodeHandle n_;
  std::string sensor_name_;
  std::vector<Segment> segments_;
  std::unordered_map<int, Segment> active_segments_;
  double last_frame_timestamp_;
  bool segment_management_active_;
  double last_frame_arrival_time_;
  double max_duration_;
  double timeout_;
  Eigen::MatrixXd Q_c_;
  Eigen::MatrixXd R_k_;
  Eigen::MatrixXd P_0_;
};

} // namespace calirad
#endif