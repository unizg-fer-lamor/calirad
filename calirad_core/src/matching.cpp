/**
 *  This file is a part of calirad.
 *
 *  Copyright (C) 2018 Juraj Persic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  calirad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <calirad_core/matching.h>

using namespace calirad;
// MatchElement member functions
MatchElement::MatchElement(std::pair<int, int> index_sensor1,
                           std::pair<int, int> index_sensor2,
                           std::pair<int, int> segments) {
  index_sensor1_ = index_sensor1;
  index_sensor2_ = index_sensor2;
  segments_ = segments;
};

std::pair<int, int> MatchElement::getIndexSensor1() { return index_sensor1_; }
std::pair<int, int> MatchElement::getIndexSensor2() { return index_sensor2_; }
std::pair<int, int> MatchElement::getIndexSegments() { return segments_; }

// InterpolatedElement member functions
InterpolatedElement::InterpolatedElement(const Eigen::MatrixXd &state_sensor1,
                                         const Eigen::MatrixXd &state_sensor2) {
  state_sensor1_ = state_sensor1;
  state_sensor2_ = state_sensor2;
};
void InterpolatedElement::getStates(Eigen::MatrixXd *state_sensor1,
                                    Eigen::MatrixXd *state_sensor2) {
  *state_sensor1 = state_sensor1_;
  *state_sensor2 = state_sensor2_;
};
void InterpolatedElement::getPositions(Eigen::Vector3d *position_sensor1,
                                       Eigen::Vector3d *position_sensor2) {
  *position_sensor1 = state_sensor1_.block<3, 1>(0, 0);
  *position_sensor2 = state_sensor2_.block<3, 1>(0, 0);
};
double InterpolatedElement::getVelocityNormResidual() {
  double residual = state_sensor1_.block<3, 1>(3, 0).norm() -
                    state_sensor2_.block<3, 1>(3, 0).norm();
  return residual;
}
double InterpolatedElement::getVelocityNormJacobian() {
  double jacobian = -(state_sensor2_.block<3, 1>(3, 0))
                         .dot(state_sensor2_.block<3, 1>(6, 0)) /
                    state_sensor2_.block<3, 1>(3, 0).norm();

  return jacobian;
}

// MatchedSegments member functions
MatchedSegments::MatchedSegments() {
  time_delay_ = 0.0;
  initial_time_delay_ = 0.0;
  exclude_outliers_ = false;
};

MatchedSegments::MatchedSegments(std::shared_ptr<TrackLog> log_sensor1,
                                 std::shared_ptr<TrackLog> log_sensor2) {
  time_delay_ = 0.0;
  initial_time_delay_ = 0.0;
  log_sensor1_ = log_sensor1;
  log_sensor2_ = log_sensor2;
  exclude_outliers_ = false;
}

void MatchedSegments::setExcludeOutliers(bool exclude_outliers) {
  exclude_outliers_ = exclude_outliers;
};
bool MatchedSegments::getExcludeOutliers() { return exclude_outliers_; };

void MatchedSegments::setInitialTimeDelay(const double &initial_time_delay) {
  initial_time_delay_ = initial_time_delay;
};

void MatchedSegments::clearMatches() { matches_.clear(); }

void MatchedSegments::simpleMatching(TrackLog &log_sensor1,
                                     TrackLog &log_sensor2) {
  int seg_number_sensor1 = log_sensor1.getSegmentNumber();
  int seg_number_sensor2 = log_sensor2.getSegmentNumber();

  for (size_t i = 0; i < seg_number_sensor1; i++) {
    std::shared_ptr<Segment> seg_sensor1 = log_sensor1.getSegmentPtr(i);
    int seg_id_sensor1 = seg_sensor1->getId();
    int seg_length_sensor1 = seg_sensor1->getSegmentLength();
    double seg_start_time_sensor1 = seg_sensor1->getStartTime();
    double seg_end_time_sensor1 = seg_sensor1->getEndTime();

    for (size_t j = 0; j < seg_number_sensor2; j++) {

      std::shared_ptr<Segment> seg_sensor2 = log_sensor2.getSegmentPtr(j);
      int seg_length_sensor2 = seg_sensor2->getSegmentLength();
      int seg_id_sensor2 = seg_sensor2->getId();
      double seg_start_time_sensor2 = seg_sensor2->getStartTime();
      double seg_end_time_sensor2 = seg_sensor2->getEndTime();

      // Segments don't match in ID
      if (!(seg_id_sensor1 == seg_id_sensor2)) {
        continue;
      }
      // Segment from sensor1 ends befor sensor2 starts
      if (seg_end_time_sensor1 < seg_start_time_sensor2) {
        continue;
      }
      // Segment from sensor2 ends before sensor1 starts
      if (seg_end_time_sensor2 < seg_start_time_sensor1) {
        continue;
      }

      bool sensor2_early = (seg_start_time_sensor2 < seg_start_time_sensor1) &&
                           (seg_end_time_sensor2 > seg_start_time_sensor1);
      bool sensor2_later = (seg_start_time_sensor2 > seg_start_time_sensor1) &&
                           (seg_end_time_sensor2 > seg_start_time_sensor1);

      int seg_element_index_sensor1 = 0;
      int seg_element_index_sensor2 = 0;

      std::pair<int, int> frame_track_index_sensor1 =
          seg_sensor1->getFrameTrackIndex(seg_element_index_sensor1);
      std::pair<int, int> frame_track_index_sensor2 =
          seg_sensor2->getFrameTrackIndex(seg_element_index_sensor2);

      double current_time_sensor1 = seg_start_time_sensor1;
      double current_time_sensor2 = seg_start_time_sensor2;

      std::shared_ptr<TrackData> current_track_ptr_sensor1 =
          log_sensor1.getTrackPtr(frame_track_index_sensor1);
      std::shared_ptr<TrackData> current_track_ptr_sensor2 =
          log_sensor2.getTrackPtr(frame_track_index_sensor2);

      std::shared_ptr<TrackFrame> current_frame_sensor1 =
          log_sensor1.getFramePtr(frame_track_index_sensor1.first);
      std::shared_ptr<TrackFrame> current_frame_sensor2 =
          log_sensor2.getFramePtr(frame_track_index_sensor2.first);

      // allign segments to starting positions
      if (sensor2_early) { // case when segment from sensor2 start earlier
        while ((seg_element_index_sensor2 < (seg_length_sensor2 - 1)) &&
               current_time_sensor2 <= current_time_sensor1) {
          seg_element_index_sensor2++;

          frame_track_index_sensor2 =
              seg_sensor2->getFrameTrackIndex(seg_element_index_sensor2);
          current_track_ptr_sensor2 =
              log_sensor2.getTrackPtr(frame_track_index_sensor2);
          current_frame_sensor2 =
              log_sensor2.getFramePtr(frame_track_index_sensor2.first);

          current_time_sensor2 = current_frame_sensor2->getTime();
        }
      } else if (sensor2_later) { // case when segment from sensor2 start later
        while ((seg_element_index_sensor1 < (seg_length_sensor1 - 1)) &&
               current_time_sensor1 <= current_time_sensor2) {
          seg_element_index_sensor1++;

          frame_track_index_sensor1 =
              seg_sensor1->getFrameTrackIndex(seg_element_index_sensor1);
          current_track_ptr_sensor1 =
              log_sensor1.getTrackPtr(frame_track_index_sensor1);
          current_frame_sensor1 =
              log_sensor1.getFramePtr(frame_track_index_sensor1.first);

          current_time_sensor1 = current_frame_sensor1->getTime();
        }
      }
      // after segment_indexes are alligned iterate through sensor1 segment
      // and find timewise closest elements in sensor2 segment

      while ((seg_element_index_sensor1 < (seg_length_sensor1 - 1)) &&
             (seg_element_index_sensor2 < (seg_length_sensor2 - 1))) {
        if (current_time_sensor2 < current_time_sensor1) {
          seg_element_index_sensor2++;

          frame_track_index_sensor2 =
              seg_sensor2->getFrameTrackIndex(seg_element_index_sensor2);
          current_track_ptr_sensor2 =
              log_sensor2.getTrackPtr(frame_track_index_sensor2);
          current_frame_sensor2 =
              log_sensor2.getFramePtr(frame_track_index_sensor2.first);

          current_time_sensor2 = current_frame_sensor2->getTime();
          continue;
        } else if (abs(current_time_sensor2 - current_time_sensor1) >
                   CALIRAD_MATCH_TIME_DIFF_THRESHOLD) {
          seg_element_index_sensor1++;

          frame_track_index_sensor1 =
              seg_sensor1->getFrameTrackIndex(seg_element_index_sensor1);
          current_track_ptr_sensor1 =
              log_sensor1.getTrackPtr(frame_track_index_sensor1);
          current_frame_sensor1 =
              log_sensor1.getFramePtr(frame_track_index_sensor1.first);

          current_time_sensor1 = current_frame_sensor1->getTime();
          continue;
        } else {
          MatchElement match_element(frame_track_index_sensor1,
                                     frame_track_index_sensor2,
                                     std::make_pair(i, j));
          matches_.push_back(match_element);
          seg_element_index_sensor1++;

          frame_track_index_sensor1 =
              seg_sensor1->getFrameTrackIndex(seg_element_index_sensor1);
          current_track_ptr_sensor1 =
              log_sensor1.getTrackPtr(frame_track_index_sensor1);
          current_frame_sensor1 =
              log_sensor1.getFramePtr(frame_track_index_sensor1.first);

          current_time_sensor1 = current_frame_sensor1->getTime();
        }
      }

    } // for with j iterator
  }   // for with i iterator
}

/** Matching is done only using approximate initial_time_delay and bounds
    on the estimated time delay to keep the correspondence number constant.
*/
void MatchedSegments::simpleGPMatching() {

  int seg_number_sensor1 = log_sensor1_->getSegmentNumber();
  int seg_number_sensor2 = log_sensor2_->getSegmentNumber();
  segment_pair_list_.clear();

  for (size_t i = 0; i < seg_number_sensor1; i++) {
    std::shared_ptr<Segment> seg_sensor1 = log_sensor1_->getSegmentPtr(i);
    int seg_id_sensor1 = seg_sensor1->getId();
    int seg_length_sensor1 = seg_sensor1->getSegmentLength();
    double seg_start_time_sensor1 = seg_sensor1->getStartTime();
    double seg_end_time_sensor1 = seg_sensor1->getEndTime();

    for (size_t j = 0; j < seg_number_sensor2; j++) {
      std::shared_ptr<Segment> seg_sensor2 = log_sensor2_->getSegmentPtr(j);
      int seg_length_sensor2 = seg_sensor2->getSegmentLength();
      int seg_id_sensor2 = seg_sensor2->getId();
      double seg_start_time_sensor2 =
          seg_sensor2->getStartTime() - initial_time_delay_;
      double seg_end_time_sensor2 =
          seg_sensor2->getEndTime() - initial_time_delay_;

      // Segments don't match in ID
      if (!(seg_id_sensor1 == seg_id_sensor2)) {
        continue;
      }
      // Segment from sensor1 ends befor sensor2 starts
      if (seg_end_time_sensor1 <= seg_start_time_sensor2) {
        continue;
      }
      // Segment from sensor2 ends before sensor1 starts
      if (seg_end_time_sensor2 <= seg_start_time_sensor1) {
        continue;
      }
      if (seg_length_sensor2 < 2 || seg_length_sensor1 < 1) {
        continue;
      }

      PairedSegments current_paired_segments(i, j);

      int seg_element_index_sensor1 = 0;
      int seg_element_index_sensor2 = 0;
      double current_time_sensor1 = seg_start_time_sensor1;
      double current_time_sensor2 = seg_start_time_sensor2;
      double next_time_sensor2 =
          seg_sensor2->getMeasurementTime(1) - initial_time_delay_;

      current_paired_segments.common_start_time_ =
          std::max(seg_start_time_sensor1, seg_start_time_sensor2) +
          time_delay_upper_bound_;
      current_paired_segments.common_end_time_ =
          std::min(seg_end_time_sensor1, seg_end_time_sensor2) +
          time_delay_lower_bound_; // deprecated, ... to be removed

      while (((current_time_sensor1 + time_delay_lower_bound_) <
              seg_start_time_sensor2) &&
             (seg_element_index_sensor1 < (seg_length_sensor1 - 1))) {
        seg_element_index_sensor1++;
        current_time_sensor1 =
            seg_sensor1->getMeasurementTime(seg_element_index_sensor1);
      }
      if (seg_element_index_sensor1 >= (seg_length_sensor1 - 1)) {
        continue;
      }

      while (((current_time_sensor1 + time_delay_lower_bound_) >
              next_time_sensor2) &&
             (seg_element_index_sensor2 < seg_length_sensor2 - 2)) {
        seg_element_index_sensor2++;
        current_time_sensor2 = next_time_sensor2;
        next_time_sensor2 =
            seg_sensor2->getMeasurementTime(seg_element_index_sensor2 + 1) -
            initial_time_delay_;
      }
      if (seg_element_index_sensor2 >= seg_length_sensor2) {
        continue;
      }

      current_paired_segments.start_index_sensor1_ = seg_element_index_sensor1;
      current_paired_segments.start_time_sensor1_ = current_time_sensor1;
      current_paired_segments.start_index_sensor2_ = seg_element_index_sensor2;
      current_paired_segments.start_time_sensor2_ =
          current_time_sensor2 + initial_time_delay_;

      while ((current_time_sensor1 + time_delay_upper_bound_) <
                 seg_end_time_sensor2 &&
             (seg_element_index_sensor1 < (seg_length_sensor1 - 1))) {
        seg_element_index_sensor1++;
        current_time_sensor1 =
            seg_sensor1->getMeasurementTime(seg_element_index_sensor1);
      }
      current_paired_segments.end_index_sensor1_ = seg_element_index_sensor1;
      current_paired_segments.end_time_sensor1_ = current_time_sensor1;

      segment_pair_list_.push_back(current_paired_segments);
    }
  }
}

void MatchedSegments::interpolateMatchesGP() {

  interpolated_matches_.clear();
  int paired_segments_number = segment_pair_list_.size();
  double total_time_delay_ = time_delay_ + initial_time_delay_;

  for (size_t i = 0; i < paired_segments_number; i++) {

    auto segment_index_sensor1 = segment_pair_list_[i].segment_index_sensor1_;
    auto segment_index_sensor2 = segment_pair_list_[i].segment_index_sensor2_;
    std::shared_ptr<Segment> seg_sensor1 =
        log_sensor1_->getSegmentPtr(segment_index_sensor1);
    std::shared_ptr<Segment> seg_sensor2 =
        log_sensor2_->getSegmentPtr(segment_index_sensor2);

    int seg_id_sensor1 = seg_sensor1->getId();

    double current_time_sensor1 = segment_pair_list_[i].start_time_sensor1_;
    double current_time_sensor2 =
        segment_pair_list_[i].start_time_sensor2_ - total_time_delay_;
    int seg_element_index_sensor1 = segment_pair_list_[i].start_index_sensor1_;
    int end_index_sensor1_ = segment_pair_list_[i].end_index_sensor1_;
    int seg_element_index_sensor2 = segment_pair_list_[i].start_index_sensor2_;

    double next_time_sensor2;
    try {
      next_time_sensor2 =
          seg_sensor2->getMeasurementTime(seg_element_index_sensor2 + 1) -
          total_time_delay_;
    } catch (std::out_of_range &ex) {
      break;
    }

    Eigen::MatrixXd state_sensor1;
    Eigen::MatrixXd state_sensor2;

    const static Eigen::IOFormat CSVFormat(Eigen::FullPrecision,
                                           Eigen::DontAlignCols, ", ", "\n");

    while (seg_element_index_sensor1 < end_index_sensor1_) {
      // First case: two measurements from sensor2 surround sensor1
      // measurement.
      if (current_time_sensor2 <= current_time_sensor1 &&
          current_time_sensor1 <= next_time_sensor2) {
        seg_sensor1->gp_regression_.getPosteriorState(
            &state_sensor1, seg_element_index_sensor1);
        seg_sensor2->gp_regression_.interpolate(
            &state_sensor2, current_time_sensor1 + total_time_delay_,
            seg_element_index_sensor2, (seg_element_index_sensor2 + 1));
        InterpolatedElement interpolated_element(state_sensor1, state_sensor2);
        interpolated_matches_.push_back(interpolated_element);
        try {
          seg_element_index_sensor1++;
          current_time_sensor1 =
              seg_sensor1->getMeasurementTime(seg_element_index_sensor1);
        } catch (std::out_of_range &ex) {
          break;
        }
        // Second case: sensor2 should move forward in time.
      } else if (current_time_sensor1 > next_time_sensor2) {
        try {
          seg_element_index_sensor2++;
          current_time_sensor2 = next_time_sensor2;
          next_time_sensor2 =
              seg_sensor2->getMeasurementTime(seg_element_index_sensor2 + 1) -
              total_time_delay_;
        } catch (std::out_of_range &ex) {
          break;
        }
        // Third case: sensor1 should move forward in time.
      } else if (current_time_sensor1 < current_time_sensor2) {
        try {
          seg_element_index_sensor1++;
          current_time_sensor1 =
              seg_sensor1->getMeasurementTime(seg_element_index_sensor1);
        } catch (std::out_of_range &ex) {
          break;
        }
      }
    }
  }
}

double MatchedSegments::getTimeDelay() { return time_delay_; };
double MatchedSegments::getInitialTimeDelay() { return initial_time_delay_; };
void MatchedSegments::setTimeDelay(const double &time_delay) {
  time_delay_ = time_delay;
};
void MatchedSegments::setTimeDelayBounds(const double &lower,
                                         const double &upper) {
  time_delay_lower_bound_ = lower;
  time_delay_upper_bound_ = upper;
}
PairedSegments::PairedSegments(){};
PairedSegments::PairedSegments(const int segment_index_sensor1,
                               const int segment_index_sensor2) {
  segment_index_sensor1_ = segment_index_sensor1;
  segment_index_sensor2_ = segment_index_sensor2;
};