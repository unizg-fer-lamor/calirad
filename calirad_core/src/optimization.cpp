/**
 *  This file is a part of calirad.
 *
 *  Copyright (C) 2018 Juraj Persic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  calirad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <calirad_core/optimization.h>

using namespace calirad;

#define CALIRAD_DEG_TO_RAD 0.0174532925

///////// Extrinsic calibration member functions
ExtrinsicCalibration::ExtrinsicCalibration(
    std::shared_ptr<MatchedSegments> matched_segments)
    : matched_segments_(matched_segments) {}

void ExtrinsicCalibration::getTranslation(std::vector<double> *translation) {
  translation->resize(translation_.size());
  *translation = translation_;
}

void ExtrinsicCalibration::getAngleAxis(std::vector<double> *angle_axis) {
  angle_axis->resize(angle_axis_.size());
  *angle_axis = angle_axis_;
};

void ExtrinsicCalibration::getAngleAxisEigen(
    Eigen::AngleAxis<double> *angle_axis_eigen) {
  *angle_axis_eigen = angle_axis_eigen_;
};
void ExtrinsicCalibration::solveSVD() {
  if (matched_segments_->interpolated_matches_.size() <= 3) {
    return;
  }
  int correspondence_number = matched_segments_->interpolated_matches_.size();
  Eigen::Vector3d center_sensor1(0, 0, 0), center_sensor2(0, 0, 0);
  Eigen::MatrixXd state_sensor1, state_sensor2;

  for (int i = 0; i < correspondence_number; ++i) {
    matched_segments_->interpolated_matches_[i].getStates(&state_sensor1,
                                                          &state_sensor2);
    center_sensor1 += state_sensor1.block(0, 0, 3, 1);
    center_sensor2 += state_sensor2.block(0, 0, 3, 1);
  }

  center_sensor1 /= (double)correspondence_number;
  center_sensor2 /= (double)correspondence_number;

  Eigen::MatrixXd S(correspondence_number, 3), D(correspondence_number, 3);
  for (int i = 0; i < correspondence_number; ++i) {
    matched_segments_->interpolated_matches_[i].getStates(&state_sensor1,
                                                          &state_sensor2);
    for (int j = 0; j < 3; ++j) {
      S(i, j) = state_sensor2(j, 0) - center_sensor2(j);
    }
    for (int j = 0; j < 3; ++j) {
      D(i, j) = state_sensor1(j, 0) - center_sensor1(j);
    }
  }

  Eigen::MatrixXd St = S.transpose();
  Eigen::Matrix3d H = St * D;
  Eigen::Matrix3d W, U, V;

  Eigen::JacobiSVD<Eigen::MatrixXd> svd;
  Eigen::MatrixXd H_alt(3, 3);

  for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 3; ++j)
      H_alt(i, j) = H(i, j);
  svd.compute(H_alt, Eigen::ComputeThinU | Eigen::ComputeThinV);
  if (!svd.computeU() || !svd.computeV()) {
    std::cerr << "decomposition error" << std::endl;
    return;
  }

  Eigen::Matrix3d Ut = svd.matrixU().transpose();
  Eigen::Matrix3d R = svd.matrixV() * Ut;
  if (R.determinant() < 0.0) {
    for (int i = 0; i < 3; ++i) {
      R(i, 2) = -R(i, 2);
    }
    std::cout << "Special case Det(R) = -1. Changed the sign of the R's third "
                 "column. Check the result!\n";
  }
  Eigen::Vector3d t = center_sensor1 - R * center_sensor2;

  std::cout << "Estimated rotation matrix:\n" << R << "\n\n";
  std::cout << "Estimated translation vector:\n" << t.transpose() << "\n\n";

  angle_axis_eigen_.fromRotationMatrix(R);
  angle_axis_.resize(angle_axis_eigen_.axis().size());
  Eigen::VectorXd::Map(&angle_axis_[0], angle_axis_eigen_.axis().size()) =
      angle_axis_eigen_.axis() * angle_axis_eigen_.angle();

  translation_.resize(t.size());
  Eigen::VectorXd::Map(&translation_[0], t.size()) = t;
}

///////// TimeDelayOpt member functions
TimeDelayOpt::TimeDelayOpt(std::shared_ptr<MatchedSegments> matched_segments)
    : matched_segments_(matched_segments) {
  time_delay_ = 0.0;
  lower_bound_ = -1.0;
  upper_bound_ = 1.0;
}

void TimeDelayOpt::setInitialTimeDelay(const double &initial_time_delay) {
  time_delay_ = initial_time_delay;
  time_delay_analytical_ = initial_time_delay;
}

void TimeDelayOpt::setBounds(double &lower_bound, double &upper_bound) {
  lower_bound_ = lower_bound;
  upper_bound_ = upper_bound;
}

void TimeDelayOpt::setOptimizationParameters(double function_tolerance,
                                             double gradient_tolerance,
                                             double parameter_tolerance) {
  std::cout << options_.function_tolerance << "," << options_.gradient_tolerance
            << "," << options_.parameter_tolerance << "\n";
  options_.minimizer_progress_to_stdout = false;
  options_.function_tolerance = function_tolerance;
  options_.gradient_tolerance = gradient_tolerance;
  options_.parameter_tolerance = parameter_tolerance;
  std::cout << options_.function_tolerance << "," << options_.gradient_tolerance
            << "," << options_.parameter_tolerance << "\n";
}

void TimeDelayOpt::addCorrespondence() {
  auto number_of_correspondences =
      matched_segments_->interpolated_matches_.size();

  problem_analytical_.AddResidualBlock(
      new AnalyticalTimeDelayCostFunction(matched_segments_), NULL,
      &time_delay_analytical_);
}

void TimeDelayOpt::solve() {
  ceres::Solve(options_, &problem_analytical_, &summary_);
}

double TimeDelayOpt::getTimeDelay() { return time_delay_analytical_; }

double TimeDelayOpt::getFinalCost() { return summary_.final_cost; }

AnalyticalTimeDelayCostFunction::AnalyticalTimeDelayCostFunction(
    std::shared_ptr<MatchedSegments> matched_segments)
    : matched_segments_(matched_segments) {
  mutable_parameter_block_sizes()->push_back(1);
  set_num_residuals(matched_segments_->interpolated_matches_.size());
}

bool AnalyticalTimeDelayCostFunction::Evaluate(const double *const *time_delay,
                                               double *residuals,
                                               double **jacobians) const {
  if (*time_delay[0] != matched_segments_->getTimeDelay()) {
    matched_segments_->setTimeDelay(*time_delay[0]);
    matched_segments_->interpolateMatchesGP();
  }

  for (size_t i = 0; i < matched_segments_->interpolated_matches_.size(); i++) {
    residuals[i] =
        matched_segments_->interpolated_matches_[i].getVelocityNormResidual();
  }

  if (jacobians != NULL) {
    for (size_t i = 0; i < matched_segments_->interpolated_matches_.size();
         i++) {
      jacobians[0][i] =
          matched_segments_->interpolated_matches_[i].getVelocityNormJacobian();
    }
  }
  return true;
};
