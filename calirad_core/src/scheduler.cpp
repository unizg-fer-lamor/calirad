/**
 *  This file is a part of calirad.
 *
 *  Copyright (C) 2018 Juraj Persic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  calirad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "calirad_core/scheduler.h"

using namespace calirad;

Scheduler::Scheduler(ros::NodeHandle nh, std::string subscribed_topic_sensor1,
                     std::string subscribed_topic_sensor2) {
  s1_to_s2_transform_.setIdentity();
  nh_ = nh;

  nh_.param("calibration_update_time", calibration_update_time_, double(10.0));
  setTimerDuration(ros::Duration(calibration_update_time_));

  nh_.param("publish_tf", publish_tf_, bool(true));

  nh_.param("time_delay_lower_bound", time_delay_lower_bound_, double(-1.0));
  nh_.param("time_delay_upper_bound", time_delay_upper_bound_, double(1.0));
  nh_.param("set_initial_time_delay", initial_time_delay_set_, bool(false));
  if (initial_time_delay_set_) {
    nh_.param("initial_time_delay", initial_time_delay_, double(0.0));
  }
  time_delay_ = 0.0;

  double segment_timeout_sensor1, segment_timeout_sensor2,
      segment_max_duration_sensor1, segment_max_duration_sensor2;
  nh_.param("segment_timeout_sensor1", segment_timeout_sensor1, double(0.3));
  nh_.param("segment_timeout_sensor2", segment_timeout_sensor2, double(0.3));
  nh_.param("segment_max_duration_sensor1", segment_max_duration_sensor1,
            double(60));
  nh_.param("segment_max_duration_sensor2", segment_max_duration_sensor2,
            double(60));
  log_sensor1_ = std::make_shared<TrackLog>(subscribed_topic_sensor1,
                                            segment_timeout_sensor1,
                                            segment_max_duration_sensor1);
  log_sensor2_ = std::make_shared<TrackLog>(subscribed_topic_sensor2,
                                            segment_timeout_sensor2,
                                            segment_max_duration_sensor2);
  std::string sensor1_name, sensor2_name;
  nh_.param("sensor1_name", sensor1_name, std::string("sensor1"));
  nh_.param("sensor2_name", sensor2_name, std::string("sensor2"));
  log_sensor1_->setSensorName(sensor1_name);
  log_sensor2_->setSensorName(sensor2_name);

  std::vector<double> Q_c_sensor1;
  std::vector<double> R_k_sensor1;
  std::vector<double> P_0_sensor1;
  std::vector<double> Q_c_sensor2;
  std::vector<double> R_k_sensor2;
  std::vector<double> P_0_sensor2;
  nh_.getParam("Q_c_sensor1", Q_c_sensor1);
  nh_.getParam("R_k_sensor1", R_k_sensor1);
  nh_.getParam("P_0_sensor1", P_0_sensor1);
  nh_.getParam("Q_c_sensor2", Q_c_sensor2);
  nh_.getParam("R_k_sensor2", R_k_sensor2);
  nh_.getParam("P_0_sensor2", P_0_sensor2);

  log_sensor1_->setQc(Q_c_sensor1);
  log_sensor1_->setRk(R_k_sensor1);
  log_sensor1_->setP0(P_0_sensor1);
  log_sensor2_->setQc(Q_c_sensor2);
  log_sensor2_->setRk(R_k_sensor2);
  log_sensor2_->setP0(P_0_sensor2);

  matched_segments_ =
      std::make_shared<MatchedSegments>(log_sensor1_, log_sensor2_);
  matched_segments_->setTimeDelayBounds(time_delay_lower_bound_,
                                        time_delay_upper_bound_);

  std::vector<std::string> v;
  fs::path p("User");
  if (exists(p) && is_directory(p))
    for (auto i = fs::directory_iterator(p); i != fs::directory_iterator(); ++i)
      if (is_directory(i->path()))
        v.push_back(i->path().filename().string());
  for (auto &s : v)
    std::cout << s << '\n';
  std::cout << "=========\n";
  sort(v.begin(), v.end());
  for (auto &s : v)
    std::cout << s << '\n';

  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);
  std::ostringstream oss;
  oss << std::put_time(&tm, "%d-%m-%Y-%H-%M-%S");
  auto str = oss.str();

  output_file_.open("output_results.txt", std::ios::app);
  // std::cout << "output_results" + str + ".txt" << std::endl;
  output_file_ << "0,0,0,0,0,0,0\n";
  output_file_.close();
  // output_file_.close();
};

void Scheduler::setTimerDuration(ros::Duration duration) {
  timer_ = nh_.createTimer(duration, &Scheduler::timerCallback, this);
}

void Scheduler::timerCallback(const ros::TimerEvent &event) {

  if (initial_time_delay_set_) {
    matched_segments_->setInitialTimeDelay(initial_time_delay_);
  } else {
    double arrival_time_diff = log_sensor2_->getLastFrameArrivalTime() -
                               log_sensor1_->getLastFrameArrivalTime();
    double timestamp_diff = log_sensor2_->getLastFrameTimestamp() -
                            log_sensor1_->getLastFrameTimestamp();
    matched_segments_->setInitialTimeDelay(timestamp_diff - arrival_time_diff);
  }

  int last_number_of_interpolated_matches =
      matched_segments_->interpolated_matches_.size();
  matched_segments_->clearMatches();
  matched_segments_->simpleGPMatching();
  matched_segments_->interpolateMatchesGP();
  int number_of_interpolated_matches =
      matched_segments_->interpolated_matches_.size();

  if (number_of_interpolated_matches &&
      last_number_of_interpolated_matches != number_of_interpolated_matches) {
    TimeDelayOpt time_delay_optimization(matched_segments_);
    time_delay_optimization.setBounds(time_delay_lower_bound_,
                                      time_delay_upper_bound_);
    time_delay_optimization.setInitialTimeDelay(time_delay_);
    time_delay_optimization.addCorrespondence();
    time_delay_optimization.setOptimizationParameters(1e-15,1e-15,1e-15);
    time_delay_optimization.solve();

    double final_cost = time_delay_optimization.getFinalCost();
    time_delay_ = time_delay_optimization.getTimeDelay();
    initial_time_delay_ = matched_segments_->getInitialTimeDelay();
    total_time_delay_ = time_delay_ + initial_time_delay_;
    std::cout << "\nEstimated time delay:\n"
              << std::setprecision(20) << total_time_delay_ << "\n\n";
    ExtrinsicCalibration extrinsic_calibration(matched_segments_);
    extrinsic_calibration.solveSVD();
    extrinsic_calibration.getTranslation(&translation_);
    extrinsic_calibration.getAngleAxis(&angle_axis_);
    extrinsic_calibration.getAngleAxisEigen(&angle_axis_eigen_);
    setTF2Transform();
    output_file_.open("output_results.txt", std::ios::app);

    output_file_ << std::setprecision(15) << total_time_delay_ << ","
                 << initial_time_delay_ << "," << time_delay_ << ","
                 << translation_[0] << "," << translation_[1] << ","
                 << translation_[2] << "," << angle_axis_[0] << ","
                 << angle_axis_[1] << "," << angle_axis_[2] << ","
                 << number_of_interpolated_matches << ","
                 << matched_segments_->segment_pair_list_.size() << ","
                 << final_cost << "\n";
    output_file_.close();
    nh_.setParam("time_delay", total_time_delay_);
    nh_.setParam("translation", translation_);
    nh_.setParam("angle_axis", angle_axis_);
  }
  if (publish_tf_) {
    broadcastEstimatedTransform();
  }
};

void Scheduler::setTF2Transform() {
  Eigen::Vector3d angle_axis, translation;

  double angle = angle_axis_eigen_.angle();
  Eigen::Vector3d axis;
  if (angle != 0.0) {
    axis = angle_axis_eigen_.axis();
  } else {
    axis << 0, 0, 1;
  }

  tf2::Vector3 tf2_axis(axis[0], axis[1], axis[2]);
  tf2::Quaternion tf2_quaternion(tf2_axis, angle);
  tf2::Vector3 tf2_translation(translation_[0], translation_[1],
                               translation_[2]);

  s1_to_s2_transform_.setOrigin(tf2_translation);
  s1_to_s2_transform_.setRotation(tf2_quaternion);
}

// forward-declaration of functions since they are defined in buffer_core.cpp,
// but not declared in buffer_core.h
namespace tf2 {
void transformMsgToTF2(const geometry_msgs::Transform &msg,
                       tf2::Transform &tf2);
void transformTF2ToMsg(const tf2::Transform &tf2,
                       geometry_msgs::TransformStamped &msg, ros::Time stamp,
                       const std::string &frame_id,
                       const std::string &child_frame_id);
} // namespace tf2

void Scheduler::broadcastEstimatedTransform() {
  tf2_ros::TransformListener tfListener(tf_buffer_);

  std::string sensor1_name, sensor2_name;
  log_sensor1_->getSensorName(&sensor1_name);
  log_sensor2_->getSensorName(&sensor2_name);

  geometry_msgs::TransformStamped bl_to_s1_transform_stamped;

  try {
    bl_to_s1_transform_stamped = tf_buffer_.lookupTransform(
        "base_link", sensor1_name, ros::Time(0), ros::Duration(0.5));
  } catch (tf2::TransformException &ex) {
    ROS_WARN("%s", ex.what());
    return;
  }

  tf2::Transform bl_to_s1_transform;
  tf2::transformMsgToTF2(bl_to_s1_transform_stamped.transform,
                         bl_to_s1_transform);

  tf2::Transform bl_to_s2_transform = bl_to_s1_transform * s1_to_s2_transform_;

  // tf2_ros::TransformBroadcaster br;
  geometry_msgs::TransformStamped bl_to_s2_transform_stamped;

  tf2::transformTF2ToMsg(bl_to_s2_transform, bl_to_s2_transform_stamped,
                         ros::Time::now(), "base_link", sensor2_name);

  br_.sendTransform(bl_to_s2_transform_stamped);
}