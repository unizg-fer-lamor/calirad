/**
 *  This file is a part of calirad.
 *
 *  Copyright (C) 2018 Juraj Persic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  calirad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "calirad_core/scheduler.h"
#include "calirad_core/track_log.h"
#include <memory>
#include <ros/ros.h>
#include <string>

int main(int argc, char *argv[]) {

  ros::init(argc, argv, "track_calibration_node");

  ros::NodeHandle nh(""), nh_param("~");

  calirad::Scheduler scheduler(nh_param, "track3_array_sensor1",
                               "track3_array_sensor2");
  ros::spin();
  ros::waitForShutdown();
}