/**
 *  This file is a part of calirad.
 *
 *  Copyright (C) 2018 Juraj Persic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  calirad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "calirad_core/track_log.h"

using namespace calirad;

// TrackData member functions
TrackData::TrackData(){

};

TrackData::TrackData(const track_msgs::Track3 &track) {
  pos_[0] = track.pos.x;
  pos_[1] = track.pos.y;
  pos_[2] = track.pos.z;
  vel_[0] = track.vel.x;
  vel_[1] = track.vel.y;
  vel_[2] = track.vel.z;
  acc_[0] = track.acc.x;
  acc_[1] = track.acc.y;
  acc_[2] = track.acc.z;
  id_ = track.id;
  status_ = track.status;
};
TrackData::TrackData(const track_msgs::Track3 &track, const double frame_time) {
  pos_[0] = track.pos.x;
  pos_[1] = track.pos.y;
  pos_[2] = track.pos.z;
  vel_[0] = track.vel.x;
  vel_[1] = track.vel.y;
  vel_[2] = track.vel.z;
  acc_[0] = track.acc.x;
  acc_[1] = track.acc.y;
  acc_[2] = track.acc.z;
  id_ = track.id;
  status_ = track.status;
  time_ = frame_time;
};

Eigen::Vector3d TrackData::getPos() const { return pos_; }
Eigen::Vector3d TrackData::getVel() const { return vel_; }
Eigen::Vector3d TrackData::getAcc() const { return acc_; }
double TrackData::getTime() const { return time_; }
int TrackData::getStatus() const { return status_; }
int TrackData::getId() const { return id_; }
void TrackData::setPos(double x, double y, double z) {
  pos_[0] = x;
  pos_[1] = y;
  pos_[2] = z;
}
void TrackData::setVel(double x, double y, double z) {
  vel_[0] = x;
  vel_[1] = y;
  vel_[2] = z;
}
void TrackData::setAcc(double x, double y, double z) {
  acc_[0] = x;
  acc_[1] = y;
  acc_[2] = z;
}
void TrackData::setStatus(int status) { status_ = status; }
void TrackData::setTime(double frame_time) { time_ = frame_time; }
void TrackData::setId(int id) { id_ = id; }

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

// TrackFrame member functions.
TrackFrame::TrackFrame() { track_number_ = 0; }

TrackFrame::TrackFrame(const track_msgs::Track3Array::ConstPtr &track_array) {
  track_number_ = 0;
  time_ = track_array->header.stamp.toSec();
  for (size_t i = 0; i < track_array->tracks.size(); i++) {
    if (track_array->tracks[i].status > 0) {
      TrackData track_data(track_array->tracks[i], time_);
      track_.push_back(track_data);
      track_number_++;
    }
  }
}

double TrackFrame::getTime() { return time_; }
void TrackFrame::setTime(double time) { time_ = time; }
int TrackFrame::getTrackNumber() { return track_.size(); }

std::shared_ptr<TrackData> TrackFrame::getTrackPtr(int track_index) {
  try {
    return std::make_shared<TrackData>(track_.at(track_index));
  } catch (std::out_of_range &ex) {
    std::cout << "\nOut of range exception caught.\n" << ex.what() << std::endl;
  }
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

// TrackLog member functions.
TrackLog::TrackLog(std::string subscribed_topic, double timeout,
                   double max_duration) {
  sub_ = n_.subscribe(subscribed_topic.c_str(), 1000, &TrackLog::trackCallback,
                      this);
  timeout_ = timeout;
  max_duration_ = max_duration;
  timer_ =
      n_.createTimer(ros::Duration(timeout_), &TrackLog::timerCallback, this);
  segment_management_active_ = false;
  last_frame_timestamp_ = 0.0;
  last_frame_arrival_time_ = 0.0;
  Q_c_.resize(3, 3);
  R_k_.resize(3, 3);
  P_0_.resize(9, 9);
}
void TrackLog::setTimeout(const double &timeout) { timeout_ = timeout; };
void TrackLog::setMaxDuration(const double &max_duration) {
  max_duration_ = max_duration;
};
void TrackLog::setSensorName(std::string &sensor_name) {
  sensor_name_ = sensor_name;
}

void TrackLog::getSensorName(std::string *sensor_name) {
  *sensor_name = sensor_name_;
}

void TrackLog::setQc(std::vector<double> &Q_c) {
  Eigen::Matrix<double, 3, 3> eye_3;
  eye_3 = Eigen::MatrixXd::Identity(3, 3);
  if (Q_c.size() == 9) {
    for (size_t i = 0; i < 3; i++) {
      for (size_t j = 0; j < 3; j++) {
        Q_c_(i, j) = Q_c[i * 3 + j];
      }
    }
  } else if (Q_c.size() == 3) {
    Q_c_ << eye_3;
    for (size_t i = 0; i < 3; i++) {
      Q_c_(i, i) = Q_c[i];
    }
  } else if (Q_c.size() == 1) {
    Q_c_ << eye_3 * Q_c[0];
  } else {
    Q_c_ << eye_3;
    std::cout << "Bad Qc size: " << Q_c.size() << ". Qc set to: \n";
    std::cout << Q_c_ << "\n";
  }
};
void TrackLog::setRk(std::vector<double> &R_k) {

  Eigen::Matrix<double, 3, 3> eye_3;
  eye_3 = Eigen::MatrixXd::Identity(3, 3);
  if (R_k.size() == 9) {
    for (size_t i = 0; i < 3; i++) {
      for (size_t j = 0; j < 3; j++) {
        R_k_(i, j) = R_k[i * 3 + j];
      }
    }
  } else if (R_k.size() == 3) {
    R_k_ << eye_3;
    for (size_t i = 0; i < 3; i++) {
      R_k_(i, i) = R_k[i];
    }
  } else if (R_k.size() == 1) {
    R_k_ << eye_3 * R_k[0];
  } else {
    R_k_ << eye_3;
    std::cout << "Bad Rk size: " << R_k.size() << ". Rk set to: \n";
    std::cout << R_k_ << "\n";
  }
};

void TrackLog::setP0(std::vector<double> &P_0) {
  if (P_0.size() == 81) {
    for (size_t i = 0; i < 9; i++) {
      for (size_t j = 0; j < 9; j++) {
        P_0_(i, j) = P_0[i * 9 + j];
      }
    }
  } else if (P_0.size() == 9) {
    Eigen::Matrix<double, 9, 9> eye_9;
    eye_9 = Eigen::MatrixXd::Identity(9, 9);
    P_0_ << eye_9;
    for (size_t i = 0; i < 9; i++) {
      P_0_(i, i) = P_0[i];
    }
  } else if (P_0.size() == 1) {
    Eigen::Matrix<double, 9, 9> eye_9;
    eye_9 = Eigen::MatrixXd::Identity(9, 9);
    P_0_ << eye_9 * P_0[0];
  } else {
    Eigen::Matrix<double, 9, 9> eye_9;
    eye_9 = Eigen::MatrixXd::Identity(9, 9);
    P_0_ << eye_9;
    std::cout << "Bad P0 size: " << P_0.size() << ". P0 set to: \n";
    std::cout << P_0_ << "\n";
  }
};

void TrackLog::timerCallback(const ros::TimerEvent &event) { manageSegments(); }

int TrackLog::getFrameNumber() { return frame_.size(); }
double TrackLog::getLastFrameTimestamp() { return last_frame_timestamp_; }
double TrackLog::getLastFrameArrivalTime() { return last_frame_arrival_time_; }

std::shared_ptr<TrackData>
TrackLog::getTrackPtr(std::pair<int, int> frame_track_index) {
  std::shared_ptr<TrackFrame> chosenframe_;
  try {
    chosenframe_ =
        std::make_shared<TrackFrame>(frame_.at(frame_track_index.first));
  } catch (std::out_of_range &ex) {
    std::cout << "\nOut of range exception caught.\n" << ex.what() << std::endl;
  }

  return chosenframe_->getTrackPtr(frame_track_index.second);
}

std::shared_ptr<TrackFrame> TrackLog::getFramePtr(int frame_index) {
  try {
    return std::make_shared<TrackFrame>(frame_.at(frame_index));
  } catch (std::out_of_range &ex) {
    std::cout << "\nOut of range exception caught Frame.\n"
              << ex.what() << std::endl;
  }
}

int TrackLog::getSegmentNumber() { return segments_.size(); }

std::shared_ptr<Segment> TrackLog::getSegmentPtr(int segment_index) {
  try {
    return std::make_shared<Segment>(segments_.at(segment_index));
  } catch (std::out_of_range &ex) {
    std::cout << "\nOut of range exception caught Segment.\n"
              << ex.what() << std::endl;
  }
}

void TrackLog::trackCallback(
    const track_msgs::Track3Array::ConstPtr &tracks_msg) {
  last_frame_timestamp_ = tracks_msg->header.stamp.toSec();
  last_frame_arrival_time_ = ros::Time::now().toSec();

  TrackFrame track_frame(tracks_msg);
  if (track_frame.getTrackNumber()) {
    int frame_index = getFrameNumber();
    frame_.push_back(track_frame);
    updateSegments(track_frame, frame_index);
  }
  manageSegments();
}

void TrackLog::updateSegments(TrackFrame &track_frame, int frame_index) {

  for (int i = 0; i < track_frame.getTrackNumber(); i++) {
    std::shared_ptr<TrackData> current_track = track_frame.getTrackPtr(i);
    int track_id = current_track->getId();
    // if track with unique id is not in the map of active segments, initialize
    // segment
    try {
      active_segments_.at(track_id).linkTrackData(current_track);
    } catch (std::out_of_range &ex) {
      std::cout << "New segment initiated in updateSegments()."
                << " " << sensor_name_ << " " << std::endl;
      Segment new_segment(current_track);
      active_segments_.insert({track_id, new_segment});
    }
  }
};

void TrackLog::manageSegments() {
  if (segment_management_active_) {
    return;
  }
  segment_management_active_ = true;
  double current_time = ros::Time::now().toSec();
  auto frame_time_diff = current_time - last_frame_arrival_time_;

  for (auto it = active_segments_.cbegin(); it != active_segments_.cend();) {
    double time_diff = last_frame_timestamp_ - it->second.getEndTime();
    double segment_length = it->second.getEndTime() - it->second.getStartTime();

    bool segment_timeout = time_diff > timeout_;
    bool frame_timeout = frame_time_diff > timeout_;
    bool segment_too_long = segment_length > max_duration_;

    if (segment_timeout || segment_too_long || frame_timeout) {
      Segment current_segment = it->second;
      current_segment.gp_regression_.model_.setRk(R_k_);
      current_segment.gp_regression_.model_.setQc(Q_c_);
      current_segment.gp_regression_.model_.setP0(P_0_);
      current_segment.GPRegression();
      segments_.push_back(current_segment);
      active_segments_.erase(it++);
    } else {
      ++it;
    }
  }
  segment_management_active_ = false;
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

// Segment member functions.

Segment::Segment(const std::shared_ptr<TrackData> track_data_ptr) {
  start_time_ = end_time_ = track_data_ptr->getTime();
  length_ = 1;
  id_ = track_data_ptr->getId();
  is_active_ = true;
  track_data_ptr_.push_back(track_data_ptr);
}

void Segment::linkTrackData(const std::shared_ptr<TrackData> track_data_ptr) {
  auto new_end_time = track_data_ptr->getTime();
  if (new_end_time > end_time_) {
    end_time_ = new_end_time;
    length_++;
    track_data_ptr_.push_back(track_data_ptr);
  }
}

double Segment::getStartTime() const { return start_time_; }
double Segment::getEndTime() const { return end_time_; }
double Segment::getMeasurementTime(const int index) {
  return gp_regression_.getMeasurementTime(index);
}

std::pair<int, int> Segment::getFrameTrackIndex(int frame_track_index) const {
  try {
    std::pair<int, int> return_value;
    return_value = frame_track_index_.at(frame_track_index);
    return return_value;

  } catch (std::out_of_range &ex) {
    std::cout << "\nOut of range exception caught Segment FrameTrack.\n"
              << ex.what() << std::endl;
  }
}

int Segment::getSegmentLength() const { return track_data_ptr_.size(); }
int Segment::getId() const { return id_; };

void Segment::GPRegression() {
  // auto start = std::chrono::high_resolution_clock::now();
  Eigen::MatrixXd measurement;
  double frame_time = 0;
  double last_frame_time = 0;
  int segment_length = getSegmentLength();

  gp_regression_.setN(segment_length);
  int wrong_detection = 0;
  for (size_t i = 0; i < segment_length; i++) {
    frame_time = track_data_ptr_[i]->getTime();
    measurement = track_data_ptr_[i]->getPos();
    gp_regression_.addMeasurement(measurement, frame_time);
  }

  // std::chrono::duration<double> elapsed_mid;
  // auto check_mid_start = std::chrono::high_resolution_clock::now();
  // auto check_1 = std::chrono::high_resolution_clock::now();

  gp_regression_.constructAinv();
  // auto check_mid_end = std::chrono::high_resolution_clock::now();
  // elapsed_mid = check_mid_end - check_mid_start;
  // check_mid_start = check_mid_end;
  // std::cout << "Matrix time: " << elapsed_mid.count() << " , ";

  gp_regression_.constructC();
  // check_mid_end = std::chrono::high_resolution_clock::now();
  // elapsed_mid = check_mid_end - check_mid_start;
  // check_mid_start = check_mid_end;
  // std::cout << "Matrix time: " << elapsed_mid.count() << " , ";

  gp_regression_.constructRinv();
  // check_mid_end = std::chrono::high_resolution_clock::now();
  // elapsed_mid = check_mid_end - check_mid_start;
  // check_mid_start = check_mid_end;
  // std::cout << "Matrix time: " << elapsed_mid.count() << " , ";

  gp_regression_.constructQinv();
  // check_mid_end = std::chrono::high_resolution_clock::now();
  // elapsed_mid = check_mid_end - check_mid_start;
  // check_mid_start = check_mid_end;
  // std::cout << "Matrix time: " << elapsed_mid.count() << " , ";

  gp_regression_.constructXprior();
  // check_mid_end = std::chrono::high_resolution_clock::now();
  // elapsed_mid = check_mid_end - check_mid_start;
  // check_mid_start = check_mid_end;
  // std::cout << "Matrix time: " << elapsed_mid.count() << " , ";

  gp_regression_.constructPpriorInv();
  // check_mid_end = std::chrono::high_resolution_clock::now();
  // elapsed_mid = check_mid_end - check_mid_start;
  // check_mid_start = check_mid_end;
  // std::cout << "Matrix time: " << elapsed_mid.count() << "\n";

  // auto check_2 = std::chrono::high_resolution_clock::now();
  gp_regression_.calculateXposterior();
  // auto end = std::chrono::high_resolution_clock::now();

  // std::chrono::duration<double> elapsed_measurements = check_1 - start;
  // std::chrono::duration<double> elapsed_matrices = check_2 - check_1;
  // std::chrono::duration<double> elapsed_posterior = end - check_2;
  // std::chrono::duration<double> elapsed_total = end - start;
  // std::cout << "Segment length: " << segment_length
  //           << "\n Time adding measurements: " <<
  //           elapsed_measurements.count()
  //           << "\n Time constructing matrices: " << elapsed_matrices.count()
  //           << "\n Time calculating posterior: " << elapsed_posterior.count()
  //           << "\n Time GP regression total: " << elapsed_total.count()
  //           << std::endl;
  return;
};
