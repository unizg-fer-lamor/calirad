#!/usr/bin/env python
import rospy
from track_msgs.msg import Track3Array
from track_msgs.msg import Track3
from visualization_msgs.msg import MarkerArray

pub = rospy.Publisher('track3_array', Track3Array, queue_size=10) 

def callback(marker_msg):
    global pub
    # rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)    
    
    if (len(marker_msg.markers) == 0):
        return
    track_array = Track3Array()
    track_array.header = marker_msg.markers[0].header
    for i in range(0,len(marker_msg.markers)):
        if marker_msg.markers[i].type > 0 :
            track = Track3()
            track.pos.x = marker_msg.markers[i].pose.position.x
            track.pos.y = marker_msg.markers[i].pose.position.y
            track.pos.z = marker_msg.markers[i].pose.position.z
            track.id = i
            track.status = 1
            track_array.tracks.append(track)
        
    pub.publish(track_array)
    
def listener():

    rospy.Subscriber("marker_array", MarkerArray, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':

    rospy.init_node('marker_array_to_track3_array', anonymous=True)
    listener()
