/**
 *  This file is a part of calirad.
 *
 *  Copyright (C) 2018 Juraj Persic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  calirad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "geometry_msgs/PoseArray.h"
#include "ros/ros.h"
#include "string.h"
#include "track_msgs/Track3Array.h"

class SubsriberPublisher {
public:
  SubsriberPublisher();
  ros::Subscriber sub_;
  ros::Publisher track3_pub_;
  ros::NodeHandle n_;
  void poseArrayCallback(const geometry_msgs::PoseArray::ConstPtr &pose_msg);

private:
  track_msgs::Track3Array track_array_;
};

SubsriberPublisher::SubsriberPublisher() {
  track3_pub_ = n_.advertise<track_msgs::Track3Array>("track3_array", 1000);
}
void SubsriberPublisher::poseArrayCallback(
    const geometry_msgs::PoseArray::ConstPtr &pose_msg) {
  int poses_num = pose_msg->poses.size();
  track_array_.tracks.resize(poses_num);
  track_array_.header = pose_msg->header;

  for (size_t i = 0; i < poses_num; i++) {
    track_array_.tracks[i].pos.x = pose_msg->poses[i].position.x;
    track_array_.tracks[i].pos.y = pose_msg->poses[i].position.y;
    track_array_.tracks[i].pos.z = pose_msg->poses[i].position.z;
    track_array_.tracks[i].id = i;
    track_array_.tracks[i].status = 1;
  }
  track3_pub_.publish(track_array_);
}

int main(int argc, char *argv[]) {
  ros::init(argc, argv, "pose_array_to_track3");

  SubsriberPublisher sp;
  sp.sub_ = sp.n_.subscribe("pose_array", 1000,
                            &SubsriberPublisher::poseArrayCallback, &sp);

  ROS_INFO("Subscribed to: %s", sp.sub_.getTopic().c_str());

  ros::spin();

  return 0;
}
