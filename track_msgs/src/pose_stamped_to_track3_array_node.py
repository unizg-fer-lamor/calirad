#!/usr/bin/env python
import rospy
from track_msgs.msg import Track3Array
from track_msgs.msg import Track3
from geometry_msgs.msg import PoseStamped

pub = rospy.Publisher('track3_array', Track3Array, queue_size=10) 

def callback(pose_stamped_msg):
    global pub
    # rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)    
    track_array = Track3Array()
    track_array.header = pose_stamped_msg.header
    track = Track3()
    track.pos.x = pose_stamped_msg.pose.position.x
    track.pos.y = pose_stamped_msg.pose.position.y
    track.pos.z = pose_stamped_msg.pose.position.z
    track.id = 0
    track.status = 1
    track_array.tracks.append(track)
    pub.publish(track_array)
    
def listener():

    rospy.Subscriber("pose_stamped", PoseStamped, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':

    rospy.init_node('pose_stamped_to_track3_array', anonymous=True)
    listener()
