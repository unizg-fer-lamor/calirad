/**
 *  This file is a part of calirad.
 *
 *  Copyright (C) 2018 Juraj Persic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  calirad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ros/ros.h"
#include "string.h"
#include "track_msgs/Track3Array.h"
#include "track_msgs/track_visualization.h"
#include "visualization_msgs/MarkerArray.h"

class SubsriberPublisher {
public:
  SubsriberPublisher(ros::NodeHandle &n, ros::NodeHandle &n_params);
  ros::Subscriber sub_;
  ros::NodeHandle n_;
  ros::NodeHandle n_params_;
  void tracks3Callback(const track_msgs::Track3Array::ConstPtr &tracks_msg);
  std::string sensor_name;

private:
  Track3Markers markers_;
};

SubsriberPublisher::SubsriberPublisher(ros::NodeHandle &n, ros::NodeHandle &n_params) { 
  n_ = n;
  n_params_ = n_params;
  markers_.initializePublisher(&n_, &n_params_); }
void SubsriberPublisher::tracks3Callback(
    const track_msgs::Track3Array::ConstPtr &tracks_msg) {
  markers_.updateTrack3Markers(*tracks_msg);
}

int main(int argc, char *argv[]) {
  ros::init(argc, argv, "tracks3_to_markers");

  ros::NodeHandle n;
  ros::NodeHandle n_params("~");
  SubsriberPublisher sp(n,n_params);
  sp.sub_ = sp.n_.subscribe("/tracks3", 1000,
                            &SubsriberPublisher::tracks3Callback, &sp);
  
  ROS_INFO("Subscribed to: %s", sp.sub_.getTopic().c_str());
  
  ros::spin();

  return 0;
}
